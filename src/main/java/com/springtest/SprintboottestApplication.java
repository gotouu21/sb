package com.springtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SprintboottestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintboottestApplication.class, args);
	}
	
	//
	@RestController
	public static class RestTest {
		@GetMapping(path="/pvz")
		public String greet() {
			return "<h1>RestTest klase veikia</h1>";
		}
		
	}

}
